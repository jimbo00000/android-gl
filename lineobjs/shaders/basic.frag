// basic.frag

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfColor;

void main()
{
    gl_FragColor = vec4(vfColor, 1.0);
}
