// basic.vert

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

attribute vec2 vPosition;
attribute vec3 vColor;

varying vec3 vfColor;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor;
    gl_Position = prmtx * mvmtx * vec4(vPosition, 0., 1.);
}
