// basicline.frag

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform vec3 uColor;

void main()
{
    gl_FragColor = vec4(uColor, 1.);
}
