// basictex.frag

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D s_texture;

varying vec2 vfTex;

void main()
{
    vec3 texCol = texture2D(s_texture, vfTex).xyz;
    gl_FragColor = vec4(texCol, 1.);
}
