// SceneInterface.h

#pragma once

#include "GL_Includes.h"
#include "SimpleScene.h"

class SceneInterface
{
public:
    SceneInterface();
    virtual ~SceneInterface();

    void initGL();
    void exitGL();
    void display();
    void timestep(double absTime, double dt);
    void setWindowSize(int w, int h) { m_winw = w; m_winh = h; }

    void onSingleTouchEvent(int pointerid, int action, int x, int y);

protected:
    int m_winw;
    int m_winh;
    float m_phase;
    SimpleScene m_scene;
    
private:
    SceneInterface(const SceneInterface&);              ///< disallow copy constructor
    SceneInterface& operator = (const SceneInterface&); ///< disallow assignment operator
};
