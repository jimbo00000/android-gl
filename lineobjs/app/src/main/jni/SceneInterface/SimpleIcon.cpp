// SimpleIcon.cpp

#include "SimpleIcon.h"
#include "ShaderFunctions.h"
#include "TextureFunctions.h"
#include "MatrixMath.h"
#include "Logging.h"
#include "shader_utils.h"
#include <string>

SimpleIcon::SimpleIcon()
: m_progBasic()
, m_texID(0)
{
}

SimpleIcon::~SimpleIcon()
{
}

void SimpleIcon::initGL()
{
    m_progBasic.initProgram("basictex");
    checkGlError("m_progBasic");

    const int dim = 128;
#ifdef __ANDROID__
    const std::string texFilename = "/sdcard/Android/data/3dcube1.raw";
#else
    const std::string texFilename = "../data/3dcube1.raw";
#endif
    m_texID = CreateTextureFromRawFile(texFilename.c_str(), dim);
}

void SimpleIcon::display(float* mview, float* proj)
{
    glUseProgram(m_progBasic.prog());

    glUniformMatrix4fv(m_progBasic.GetUniLoc("mvmtx"), 1, false, mview);
    glUniformMatrix4fv(m_progBasic.GetUniLoc("prmtx"), 1, false, proj);

    // The Galaxy Tab 4 Vivante device does not like GL_INT type here, but GL_FLOAT is OK.
    const float sz = 100.f;
    const GLfloat QuadVerts[] = {
        0.f, 0.f,
        sz, 0.f,
        sz, sz,
        0.f, sz,
   };

    const GLfloat QuadCols[] = {
        0.f, 0.f,
        1.f, 0.f,
        1.f, 1.f,
        0.f, 1.f,
    };

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texID);
    glUniform1i(m_progBasic.GetUniLoc("s_texture"), 0);

    glVertexAttribPointer(m_progBasic.GetAttrLoc("vPosition"), 2, GL_FLOAT, GL_FALSE, 0, QuadVerts);
    glVertexAttribPointer(m_progBasic.GetAttrLoc("vTexCoord"), 2, GL_FLOAT, GL_FALSE, 0, QuadCols);
    glEnableVertexAttribArray(m_progBasic.GetAttrLoc("vPosition"));
    glEnableVertexAttribArray(m_progBasic.GetAttrLoc("vTexCoord"));

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    checkGlError("glDrawArrays");
}
