// LineObj.cpp

#include "LineObj.h"
#include "StringFunctions.h"
#include <fstream>

LineObj::LineObj()
 : m_verts()
 , m_groups()
 , m_lineprog()
{
}

LineObj::~LineObj()
{
}

///@return 0 for success, non-zero otherwise
int LineObj::loadFromFile(const std::string& filename)
{
    std::ifstream infile(filename.c_str());
    if (infile.is_open() == false)
    {
        return 1;
    }

    int basevert = 0;
    int numverts = 0;

    std::string line;
    while (std::getline(infile, line))
    {
        std::vector<std::string> toks = split(line, ' ');
        if (toks.size() < 2)
            continue;

        const std::string& t = toks[0];
        if (t.empty())
            continue;

        switch(t[0])
        {
        default:
            break;

        case 'o':
            {
                numverts = m_idxs.size() - basevert - 1;
                if (!m_verts.empty())
                {
                    lineGroup g = {numverts, basevert};
                    m_groups.push_back(g);
                }
                basevert = m_idxs.size();
            }
            break;

        case 'v':
            if (toks.size() >= 4)
            {
                for (int i = 0; i < 3; ++i)
                {
                    float f = static_cast<float>(strtod(toks[i].c_str(), NULL));
                    m_verts.push_back(f);
                }
            }
            break;

        case 'l':
            {
                for (std::vector<std::string>::const_iterator it = toks.begin() + 1;
                    it != toks.end();
                    ++it)
                {
                    const std::string& s = *it;
                    const int i = static_cast<int>(strtol(s.c_str(), NULL, 0));
                    m_idxs.push_back(i);
                }
            }
            break;
        }
    }

    numverts = m_idxs.size() - basevert - 1;
    lineGroup g = { numverts, basevert };
    m_groups.push_back(g);

    return 0;
}

void LineObj::initGL()
{
    m_lineprog.initProgram("basicline");
    m_lineprog.bindVAO();

    if (m_verts.empty())
        return;
    if (m_idxs.empty())
        return;

    GLuint vertVbo = 0;
    glGenBuffers(1, &vertVbo);
    m_lineprog.AddVbo("vPosition", vertVbo);
    glBindBuffer(GL_ARRAY_BUFFER, vertVbo);
    glBufferData(GL_ARRAY_BUFFER, m_verts.size() *sizeof(GLfloat), &m_verts[0], GL_STATIC_DRAW);
    glVertexAttribPointer(m_lineprog.GetAttrLoc("vPosition"), 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(m_lineprog.GetAttrLoc("vPosition"));

    GLuint quadVbo = 0;
    glGenBuffers(1, &quadVbo);
    m_lineprog.AddVbo("elements", quadVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_idxs.size() * sizeof(GLuint), &m_idxs[0], GL_STATIC_DRAW);

    glBindVertexArray(0);
}

void LineObj::exitGL()
{
}

void LineObj::display(float* mview, float* proj)
{
    const ShaderWithVariables& swv = m_lineprog;
    glUseProgram(swv.prog());

    glUniformMatrix4fv(swv.GetUniLoc("mvmtx"), 1, false, mview);
    glUniformMatrix4fv(swv.GetUniLoc("prmtx"), 1, false, proj);
    glUniform3f(swv.GetUniLoc("uColor"), 1.f, 0.f, 0.f);

    swv.bindVAO();
    for (std::vector<lineGroup>::const_iterator it = m_groups.begin();
        it != m_groups.end();
            ++it)
    {
        const lineGroup& g = *it;
        const int start = g.base;
        const int end = start + g.count;
        glDrawElements(GL_LINE_STRIP,
            g.count,
            GL_UNSIGNED_INT,
            (void*)(start * sizeof(GLuint)));
    }
    glBindVertexArray(0);
}
