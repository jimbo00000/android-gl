// SceneInterface.cpp

#include "SceneInterface.h"
#include <math.h>

SceneInterface::SceneInterface()
: m_winw(800)
, m_winh(800)
, m_phase(0.f)
, m_scene()
{
}

SceneInterface::~SceneInterface()
{
}

void SceneInterface::initGL()
{
    m_scene.initGL();
}

void SceneInterface::exitGL()
{
    //m_scene.exitGL();
}

void SceneInterface::display()
{
    m_scene.display(m_winw, m_winh);
}

void SceneInterface::timestep(double absTime, double dt)
{
    m_phase = absTime;
}

void SceneInterface::onSingleTouchEvent(int pointerid, int action, int x, int y)
{
    //LOGI("onSingleTouchEvent( @%f: %d, %d, %f, %f)\n", g_timer.seconds(), pointerid, action, x, y);
    m_scene.OnSingleTouch(pointerid, action, x, y);
}
