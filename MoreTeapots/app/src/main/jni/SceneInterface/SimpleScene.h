// SimpleScene.h

#pragma once

#include "GL_Includes.h"
#include "SimpleIcon.h"
#include "TouchPoints.h"
#include "FPSTimer.h"

class SimpleScene
{
public:
    SimpleScene();
    virtual ~SimpleScene();

    void initGL();
    void setWindowSize(int w, int h) { m_winw = w; m_winh = h; }
    void display(int winw, int winh);

    void OnSingleTouch(int pointerid, int action, int x, int y);

protected:
    void _DrawIcon(int winw, int winh);
    void _DrawText(int winw, int winh);
    SimpleIcon g_si;
    FPSTimer m_fps;
    Timer m_logDumpTimer;
    int m_winw;
    int m_winh;
    int m_iconx;
    int m_icony;
    float m_iconScale;

    // Motion event states
    bool m_holding;
    unsigned int m_holdingMask;
    std::vector<touchState> m_pointerStates;
    TouchPoints m_tp;
    std::pair<touchState, touchState> m_pinchStart;
    float m_scaleAtPinchStart;

private:
    SimpleScene(const SimpleScene&);              ///< disallow copy constructor
    SimpleScene& operator = (const SimpleScene&); ///< disallow assignment operator
};
