// SimpleScene.cpp

#include "SimpleScene.h"

#include "AndroidTouchEnums.h"
#include "FontMgr.h"
#include "FontRenderer.h"
#include "MatrixMath.h"
#include "VectorMath.h"
#include "Logging.h"
#include <sstream>

SimpleScene::SimpleScene()
: g_si()
, m_line()
, m_fps()
, m_logDumpTimer()
, m_iconx(200)
, m_icony(200)
, m_iconScale(1.f)
, m_holding(false)
, m_holdingMask(0)
, m_pointerStates(8)
, m_scaleAtPinchStart(1.f)
{
}

SimpleScene::~SimpleScene()
{
}

void SimpleScene::initGL()
{
    g_si.initGL();
    m_tp.initGL();

    const Language lang = USEnglish;
    FontMgr::Instance().LoadLanguageFonts(lang);

#ifdef __ANDROID__
    const std::string dataHome = "/sdcard/Android/data/";
#else
    const std::string dataHome = "../data/";
#endif
    const std::string lineFilename = dataHome + "myDrawing3.obj";
    m_line.loadFromFile(lineFilename);
    m_line.initGL();
}

void SimpleScene::_DrawIcon(int winw, int winh)
{
    float mview[16];
    float proj[16];
    // Set up projection matrix for Pixel coordinates - lower left origin
    glhOrtho(proj,
        0.f, static_cast<float>(winw),
        static_cast<float>(winh),0.f, 
        -1.f, 1.f);
    MakeIdentityMatrix(mview);
    glhTranslate(mview, m_iconx, m_icony, 0);
    const float s = m_iconScale * m_scaleAtPinchStart;
    glhScale(mview, s, s, s);

    g_si.display(mview, proj);
}

void SimpleScene::_DrawText(int winw, int winh)
{
    float mview[16];
    float proj[16];

    MakeIdentityMatrix(mview);
    // Flip window coordinates vertically so origin is upper-left
    glhOrtho(proj,
        0.f, static_cast<float>(winw),
        static_cast<float>(winh), 0.f,
        -1.f, 1.f);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    const FontRenderer* pFont24 = FontMgr::Instance().GetFontOfSize(24);
    if (pFont24 != NULL)
    {
        const float3 col = {1.f, .5f, .5f};
        const bool doKerning = true;
        {
            std::wstringstream oss;
            oss << L"HelloGL2 on ";
#ifdef __ANDROID__
            oss << "Android";
#else
            oss << "Desktop";
#endif
            pFont24->DrawWString(
                oss.str().c_str(),
                10,
                10,
                col,
                proj,
                doKerning);
        }
        {
            std::ostringstream oss;
            oss << m_fps.GetFPS() << " fps";
            pFont24->DrawString(
                oss.str().c_str(),
                10,
                50,
                col,
                proj,
                doKerning);
        }
    }
}

void SimpleScene::display(int winw, int winh)
{
    m_fps.OnFrame();

#if 1
    // Log fps at regular intervals
    const float dumpInterval = 1.f;
    if (m_logDumpTimer.seconds() > dumpInterval)
    {
        LOG_INFO("Frame rate: %d fps", static_cast<int>(m_fps.GetFPS()));
        m_logDumpTimer.reset();
    }
#endif

    glViewport(0, 0, winw, winh);
    glClearColor(0,0,0,0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    {
        float mvmtx[16];
        float prmtx[16];
        MakeIdentityMatrix(mvmtx);
        glhTranslate(mvmtx, 0.f, 0.f, -10.f);
        glhRotate(mvmtx, 90.f, 0.f, 1.f, 0.f);
        glhRotate(mvmtx, 90.f, 1.f, 0.f, 0.f);

        glhPerspectivef2(prmtx,
                         80.f,
                         static_cast<float>(winw) / static_cast<float>(winh),
                         .01f, 1000.f);

        m_line.display(mvmtx, prmtx);
    }

#ifndef __ANDROID__
    // Draw pointer states
    glPointSize(10.f);
    glDisable(GL_DEPTH_TEST);
    float mview[16];
    float proj[16];
    MakeIdentityMatrix(mview);
    // Flip window coordinates vertically so origin is upper-left
    glhOrtho(proj,
        0.f, static_cast<float>(winw),
        static_cast<float>(winh), 0.f,
        -1.f, 1.f);

    m_tp.display(mview, proj, m_pointerStates);
#endif
}

int getNumPointersDown(int mask)
{
    int num = 0;
    const int maxPointers = 10;
    for (int i=0; i<maxPointers; ++i)
    {
        const int flag = 1 << i;
        if ((mask & flag) != 0)
            ++num;
    }
    return num;
}

void SimpleScene::OnSingleTouch(int pointerid, int action, int x, int y)
{
    const int pointerflag = 1 << pointerid;
    const int actionflag = action & 0xff;

    touchState& ts = m_pointerStates[pointerid];
    ts.state = actionflag;
    ts.x = x;
    ts.y = y;

    if (actionflag == ActionDown) {
        m_holdingMask |= pointerflag;
    }
    else if (actionflag == ActionPointerDown) {
        m_holdingMask |= pointerflag;
        // Get current pinch size
    }
    else if (actionflag == ActionUp) {
        m_holdingMask &= ~pointerflag;
    }
    else if (actionflag == ActionPointerUp) {
        m_holdingMask &= ~pointerflag;
        if (getNumPointersDown(m_holdingMask) == 1)
        {
            m_scaleAtPinchStart *= m_iconScale;
            m_iconScale = 1.f;
        }
    }
    else if (actionflag == ActionMove) {
        // Save pointer's location
        printf("M<ove: %d\n", pointerid);
    }


    // Handle a pinch event
    if (actionflag == ActionPointerDown)
    {
        if (getNumPointersDown(m_holdingMask) == 2)
        {
            m_pinchStart.first = m_pointerStates[0];
            m_pinchStart.second = m_pointerStates[1];
        }
    }
    else if (actionflag == ActionMove)
    {
        if (getNumPointersDown(m_holdingMask) == 2)
        {
            const touchState& t0 = m_pinchStart.first;
            const touchState& t1 = m_pinchStart.second;
            const float3 a0 = {static_cast<float>(t0.x),static_cast<float>(t0.y),0};
            const float3 a1 = {static_cast<float>(t1.x),static_cast<float>(t1.y),0};
            const float initialDist = length(a1 - a0);
            const touchState& u0 = m_pointerStates[0];
            const touchState& u1 = m_pointerStates[1];
            const float3 b0 = {static_cast<float>(u0.x),static_cast<float>(u0.y),0};
            const float3 b1 = {static_cast<float>(u1.x),static_cast<float>(u1.y),0};
            const float curDist = length(b1 - b0);
            if (initialDist != 0.f)
            {
                m_iconScale = curDist / initialDist;
            }
        }
    }



    if (m_holdingMask == 1)
    {
        if (getNumPointersDown(m_holdingMask) == 1)
        {
            if ((actionflag == ActionDown) || (actionflag == ActionMove))
            {
                m_iconx = x;
                m_icony = y;
            }
        }
    }

}
