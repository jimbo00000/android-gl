// cpp_interface.cpp

#include "cpp_interface.h"

#include "SimpleScene.h"
#include "shader_utils.h"
#include "Logging.h"

int g_winw;
int g_winh;
SimpleScene g_scene;
Timer g_timer;

bool initScene()
{
    LOGI("initScene()");
    printSomeGLInfo();

    g_scene.initGL();

    return true;
}

void surfaceChangedScene(int w, int h)
{
    LOGI("setupGraphics(%d, %d)", w, h);
    g_winw = w;
    g_winh = h;
}

void drawScene()
{
    g_scene.display(g_winw, g_winh);
}

void onSingleTouchEvent(int pointerid, int action, float x, float y)
{
    LOGI("onSingleTouchEvent( @%f: %d, %d, %f, %f)\n", g_timer.seconds(), pointerid, action, x, y);
    g_scene.OnSingleTouch(pointerid, action, x, y);
}
