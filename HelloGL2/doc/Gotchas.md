Gotchas in Android Development
===============================

## Header errors related to __ANDROID__ and __ANDROID_API__

Using the `__ANDROID__` and `__ANDROID_API__` macros can be a handy way to distinguish between build types in code. However, exactly where and how they are defined is not quite straightforward. A file's inclusion within another file can determine whether the macro is defined correctly or will throw an error. Sometimes the behavior seems unpredictable.

A "Clean Project" operation may sometimes help here.



## TortoiseGit forgets which case the directory is in

Occasionally, the TortoiseGit explorer plugin for Windows may forget the proper case of the path name it's in. This will result in an erroneously populated commit dialog, with all your files having been deleted and re-aded under a directory with flattened case. This seems to occur when adding a file to the ignore list.

When this happens, back the explorer directory out to the parent of the project and then click back in. The case should have corrected itself and commit can proceed normally.


## Including the STL

in `build.gradle`:
```
     android.ndk {
        stl = "stlport_static"
    }
```


## Opening files from device local storage using C++ and the NDK

http://stackoverflow.com/questions/1992953/file-operations-in-android-ndk

Add the line
`<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>`
to AndroidManifest.xml

The directory `/sdcard/Android/data/` on Android maps to 
`This PC\Software _group_02\Tablet\Android\data` on the Windows Mass Storage Device drive.

If the driver doesn't provide the mass storage drive mapping interface for Windows, files can be pushed to the device using an adb command:

`adb push /path/to/local/file /mnt/sdcard/path/to/file`

http://log.amitshah.net/2012/05/using-adb-to-copy-files-to-from-your-android-device/


## Surface gets re-created on every change of screen orientation

`GLSurfaceView.Renderer` class should call `onSurfaceCreated` only once on launch of the app, then call `onSurfaceChanged` every time the screen orientation changes due to device rotation. However, with default settings the app calls *both* functions on every rotation, leading to unnecessary re-creation of resources.

The problem is solved by adding a flag to the activity tag in AndroidManifest.xml:

`android:configChanges="orientation|keyboardHidden|screenSize`

http://stackoverflow.com/a/19067258


## Vivante GPU does not recognize GL_INT attribute type

The Vivante GPU in the Galaxy Tab 4 will throw a `GL_INVALID_ENUM, 0x0500` error and print `gl2mERROR: result=0x0500 @ glshVertexAttribPointer(560)` when attempting to use `GL_INT` type. Casting the attribute values and passing `GL_FLOAT` to `glVertexAttribPointer` works.


## Fonts do not display when ADB is not attached

Deploying and launching the app from Android Studio, the fonts work fine although errors are thrown in the log output. But when launching the same app from the tablet itself(by tapping its icon), the fonts don't work and errors are shown on ShaderMgr::GetShaderByName.

What is the difference between invoking an app via adb and tapping it from the tablet's screen?

This seems to be connected to the use of the static singleton ShaderMgr to mediate creation of shaders - calling makeShaderByName directly eliminates the problem. Does this have something to do with threading and the thread residency of the current GL context?

### App Instance already running

The font displays properly and no GL errors are thrown with the singleton if the already running instance of the app is quit before a new instance is re-launched from the Android UI. This can be done with some sort of UI widget (the stacked squares hard button on Samsung's Galaxy Tab, the square soft button on the Intrinsyc dev board).

The font may also disappear when the device is put into sleep or suspend. Is there an Android event we can add to the manifest to re-create context on? Why just for the fonts - the static singleton?

### atof -> strtod
Some devices lack the atof() function. Replace it with `static_cast<float>(strtod(val, NULL));`.

### VBO binding causing error output on glDrawElements
On the Samsung/Vivante device, VBO bindings seem to be more strict than on the NVIDIA or Qualcomm devices. Leaving VBOs bound at init time causes errors not seen elsewhere which can be fixed by un-binding any VBOs after each use.

