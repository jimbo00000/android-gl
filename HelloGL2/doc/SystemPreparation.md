System Preparation
================

## Prerequisites

To build the code in this repository for Android or desktop OS, your system needs the following software installed:

- Python [2.7.10](https://www.python.org/downloads/)

Android:  | Desktop OS:
----------|------------
- [Android Studio 1.4+](http://developer.android.com/sdk/index.html)  | - CMake [2.8+](https://cmake.org/download/)
- [NDK](https://developer.android.com/ndk/) bundle                    | - glfw [32-bit 3.1.2](http://www.glfw.org/download.html)

For cloning and committing changes to the repo:
- Git - [TortoiseGit](https://tortoisegit.org/download/) for Windows is an excellent client

Use the default installation paths for all software; it just makes finding things easier on different machines.

Once python is installed, add it to the system's path. On Windows, this is done by right clicking "This PC"/Computer and choosing properties, then "Advanced system settings", "Environment Variables...", then choosing PATH and clicking the "Edit..." button.

![AddPythonToPath.PNG](AddPythonToPath.PNG "1")

Download glfw [32-bit 3.1.2](http://www.glfw.org/download.html) and place it in the directory `C:\lib\`.
