Build Instructions
==============

## Windows Desktop build

Clone the repository using TortoiseGit or download a zip file and place it in a familiar directory where you will be building and (optionally) staging your work.

To build the software for Windows, the first step is to launch **CMake (cmake-gui)**. Enter the fully-qualified path name of the **CMakeLists.txt** file for the project to build in the "Where is the source code:" field. In the "Where to build the binaries:" field, append `/build-cmake` to the path name:

![CMakeWhereIsTheSource.PNG](CMakeWhereIsTheSource.PNG "2")

Then click *"Configure"*. If the directory does not yet exist, click Yes to create it.

Choose the generator/compiler system you would like to use: Visual Studio 11 is a fine choice.

![SpecifyAGenerator.PNG](SpecifyAGenerator.PNG "3")

Output will scroll in the bottom window, click *Configure* again until the items are no longer red.

![ConfigComplete.PNG](ConfigComplete.PNG "4")

Then click Generate. A .sln file will be generated in the build-cmake directory, double-click it to launch Visual Studio. Right-click the HelloGL2-Desktop project and choose *"Set as StartUp project"*. Press *F7* or *control-shift-b* or choose *BUILD-->Build Solution*.

![SetAsStartupProject.PNG](SetAsStartupProject.PNG "5")

Press *F5* or choose *DEBUG-->Start Debugging* to run the app.


## Android build
- Open the HelloGL2 project
- Plugin an Android device
- Push Files to device: `DeployDataToDevice.bat`
- Press shift + F10 to deploy and run the project on the attached device
