// basictex.vert

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

attribute vec2 vPosition;
attribute vec2 vTexCoord;

varying vec2 vfTex;

uniform mat4 mvmtx;
uniform mat4 prmtx;

void main()
{
    vfTex = vTexCoord;
    gl_Position = prmtx * mvmtx * vec4(vPosition, 0., 1.);
}
