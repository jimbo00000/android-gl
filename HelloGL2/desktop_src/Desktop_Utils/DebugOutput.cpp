// DebugOutput.cpp

#ifdef _WIN32
#include "DebugOutput.h"
#include <sstream>

void OutputPrint(char* format, ...)
{
    const unsigned int bufSz = 2048;
    char buffer[bufSz];

    va_list args;
    va_start(args, format);
    vsprintf_s(buffer, bufSz, format, args);
    va_end(args);

#if 0
    printf("%s\n", buffer);
#else
    rfxLineOut(buffer);
#endif
}
#endif
