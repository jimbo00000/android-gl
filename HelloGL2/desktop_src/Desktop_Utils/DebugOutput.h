// DebugOutput.h

#pragma once

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>

#define rfxLineOut( S )                 	\
	std::wostringstream osw;                \
    osw << S << std::endl;                  \
	OutputDebugString(osw.str().c_str());


void OutputPrint(char* format, ...);
#endif
