// glfw_main.cpp

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined(_WIN32)
#  include <Windows.h>
#endif

#include "GL_Includes.h"
#include <GLFW/glfw3.h>

#include "cpp_interface.h"
#include "AndroidTouchEnums.h"
#include "TouchReplayer.h"
#include "Timer.h"

GLFWwindow* g_pWindow = NULL;
int winw = 800;
int winh = 1000;
TouchReplayer g_trp;

void initGL()
{
    initScene();
}

void display()
{
    drawScene();
}

void keyboard(GLFWwindow* pWindow, int key, int codes, int action, int mods)
{
    (void)pWindow;
    (void)codes;

    if (action == GLFW_PRESS)
    {
    switch (key)
    {
        default:
            break;

        case GLFW_KEY_F1:
            std::swap(winw, winh);
            surfaceChangedScene(winw, winh);
            glfwSetWindowSize(g_pWindow, winw, winh);
            ///@todo call surfaceChanged(winw, winh);
            glViewport(0, 0, winw, winh);
            break;

        case GLFW_KEY_ESCAPE:
            glfwTerminate();
            exit(0);
            break;
    }
    }
}

void mouseDown(GLFWwindow* pWindow, int button, int action, int mods)
{
    (void)pWindow;
    (void)mods;

    double xd, yd;
    glfwGetCursorPos(pWindow, &xd, &yd);
    const float x = static_cast<float>(xd);
    const float y = static_cast<float>(yd);

    if (button == GLFW_MOUSE_BUTTON_1)
    {
        if (action == GLFW_PRESS)
        {
            onSingleTouchEvent(0, ActionDown, x, y);
        }
        else if (action == GLFW_RELEASE)
        {
            onSingleTouchEvent(0, ActionUp, x, y);
        }
    }
}

void mouseMove(GLFWwindow* pWindow, double xd, double yd)
{
    (void)pWindow;
    const float x = static_cast<float>(xd);
    const float y = static_cast<float>(yd);
    onSingleTouchEvent(0, ActionMove, x, y);
}

#define LOG_INFO printf

void printGLContextInfo(GLFWwindow* pW)
{
    // Print some info about the OpenGL context...
    const int l_Major = glfwGetWindowAttrib(pW, GLFW_CONTEXT_VERSION_MAJOR);
    const int l_Minor = glfwGetWindowAttrib(pW, GLFW_CONTEXT_VERSION_MINOR);
    const int l_Profile = glfwGetWindowAttrib(pW, GLFW_OPENGL_PROFILE);
    if (l_Major >= 3) // Profiles introduced in OpenGL 3.0...
    {
        if (l_Profile == GLFW_OPENGL_COMPAT_PROFILE)
        {
            LOG_INFO("GLFW_OPENGL_COMPAT_PROFILE");
        }
        else
        {
            LOG_INFO("GLFW_OPENGL_CORE_PROFILE");
        }
    }
    (void)l_Minor;
    LOG_INFO("OpenGL: %d.%d\n", l_Major, l_Minor);
    LOG_INFO("Vendor: %s\n", reinterpret_cast<const char*>(glGetString(GL_VENDOR)));
    LOG_INFO("Renderer: %s\n", reinterpret_cast<const char*>(glGetString(GL_RENDERER)));
}

// OpenGL debug callback
void APIENTRY myCallback(
    GLenum source, GLenum type, GLuint id, GLenum severity,
    GLsizei length, const GLchar *msg,
    const void *data)
{
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
    case GL_DEBUG_SEVERITY_MEDIUM:
    case GL_DEBUG_SEVERITY_LOW:
        LOG_INFO("[[GL Debug]] %x %x %x %x %s\n", source, type, id, severity, msg);
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        break;
    }
}

int main(int argc, char** argv)
{
    LOG_INFO("Compiled against GLFW %i.%i.%i\n",
        GLFW_VERSION_MAJOR,
        GLFW_VERSION_MINOR,
        GLFW_VERSION_REVISION);
    int major, minor, revision;
    glfwGetVersion(&major, &minor, &revision);
    LOG_INFO("Running against GLFW %i.%i.%i\n", major, minor, revision);
    LOG_INFO("glfwGetVersionString: %s\n", glfwGetVersionString());

    GLFWwindow* l_Window = NULL;
    if (!glfwInit())
    {
        exit(EXIT_FAILURE);
    }

    // Context setup - before window creation
    glfwWindowHint(GLFW_DEPTH_BITS, 16);

    bool useGLES = true;
#ifdef _DEBUG
    useGLES = false; // debug won't work with GLES
#endif

    if (useGLES)
    {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    }
    else
    {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    }

#ifdef _DEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    l_Window = glfwCreateWindow(winw, winh, "GLFW desktop GL", NULL, NULL);
    if (!l_Window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    g_pWindow = l_Window;

    glfwSetKeyCallback(l_Window, keyboard);
    glfwSetMouseButtonCallback(l_Window, mouseDown);
    glfwSetCursorPosCallback(l_Window, mouseMove);
    glfwMakeContextCurrent(l_Window);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return -1;
    }

#ifdef _DEBUG
    // Debug callback initialization
    // Must be done *after* glew initialization.
    glDebugMessageCallback(myCallback, NULL);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
    glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
        GL_DEBUG_SEVERITY_NOTIFICATION, -1 , "Start debugging");
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif

    printGLContextInfo(l_Window);
    initGL();
    surfaceChangedScene(winw, winh);

    g_trp.LoadTouchLogFromFile("touchlog.txt");

    glfwSwapInterval(1);
    Timer playbackTimer;
    while (!glfwWindowShouldClose(l_Window))
    {
        g_trp.PlaybackRecentEvents(playbackTimer.seconds(), onSingleTouchEvent);
        glfwPollEvents();
        display();
        glfwSwapBuffers(l_Window);
    }

    glfwDestroyWindow(l_Window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
