// GL_Includes.h

#pragma once

#ifdef __ANDROID__
#include <GLES/gl.h>
#else
#include <glad/glad.h>
#endif
