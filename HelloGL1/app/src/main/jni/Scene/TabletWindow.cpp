// TabletWindow.cpp

#include "TabletWindow.h"

#include "AndroidTouchEnums.h"
#include "MatrixMath.h"
#include "VectorMath.h"
#include "Logging.h"
#include <sstream>
#include <fstream>

TabletWindow::TabletWindow()
: m_fps()
, m_logDumpTimer()
, m_iconx(20)
, m_icony(240)
, m_iconScale(1.f)
, m_glVersion()
, m_glRenderer()
, m_glSLVersion()
, m_holding(false)
, m_holdingMask(0)
, m_scaleAtPinchStart(1.f)
, m_lastTouchPoint(0,0)
, m_chassisYaw(0.f)
, m_chassisYawAtTouch(0.f)
{
    m_chassisPos.x = 0.f;
    m_chassisPos.y = -.6f;
    m_chassisPos.z = -5.f;
}

TabletWindow::~TabletWindow()
{
}

void TabletWindow::initGL()
{
    const std::string v(reinterpret_cast<const char*>(glGetString(GL_VERSION)));
    m_glVersion = v;

    const std::string r(reinterpret_cast<const char*>(glGetString(GL_VENDOR)));
    m_glRenderer = r;

    const std::string s(reinterpret_cast<const char*>(glGetString(GL_RENDERER)));
    m_glSLVersion = s;
}


static float r = 0.f;
static float g = 0.f;
static float b = 0.f;
void TabletWindow::display(int winw, int winh)
{
    glViewport(0, 0, winw, winh);
    glClearColor(r, g, b, 0.f);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}

void TabletWindow::timestep(double absT, double dt)
{
    m_fps.OnFrame();
    r = sin(absT);
    g = sin(1.3*absT);
    b = sin(1.7*absT);

#if 1
    // Log fps at regular intervals
    const float dumpInterval = 1.f;
    if (m_logDumpTimer.seconds() > dumpInterval)
    {
        LOG_INFO("Frame rate: %d fps", static_cast<int>(m_fps.GetFPS()));
        m_logDumpTimer.reset();
}
#endif
}

int getNumPointersDown(int mask)
{
    int num = 0;
    const int maxPointers = 10;
    for (int i=0; i<maxPointers; ++i)
    {
        const int flag = 1 << i;
        if ((mask & flag) != 0)
            ++num;
    }
    return num;
}

void TabletWindow::OnSingleTouch(int pointerid, int action, int x, int y)
{
    const int pointerflag = 1 << pointerid;
    const int actionflag = action & 0xff;
}

void TabletWindow::OnWheelEvent(double dx, double dy)
{
    m_chassisPos.z += .4f * dy;
}
